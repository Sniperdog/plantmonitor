import React, { Component } from 'react';
// import './actualMonitor.css';
//import './actualMonitor.css';
import styled from 'styled-components';
import Table from './Table.js';
import { Row, Col } from 'antd';

// function readJSON() {
//     var request = new XMLHttpRequest();
//     request.open("GET", "monitorData.json", false);
//     request.send(null)
//     var my_JSON_object = JSON.parse(request.responseText);
//     alert(my_JSON_object.result[0]);
// };

const sampleData = {
  "WINGS": {
    "Planning": [
      {
        "Open Waves": "2"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "HTLS": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "COURT": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "App1+2": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "HTLS": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "App3": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  },
  "FTW": {
    "Planning": [
      {
        "Open Waves": "4"
      }
    ],
    "Inbound": [
      {
        "Received": "400,000"
      }
    ],
    "Processing": [
      {
        "Units": "543,210"
      }
    ],
    "Shipping": [
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
  }
};

class Monitor extends Component {
  state = {
    data: sampleData
  };

  fetchData = () => {
    // fetch('full url')
    //   .then(resp => resp.json())
    //   .then(resp => {
    //     // handle json
    //     this.setState({
    //        data: resp
    //     })
    //   })
    //   .catch(error => {
    //     console.error(error);
    //     // other stuff
    //   })
  }

  componentDidMount() {
    this.interval = setInterval(this.fetchData, 30000); // refresh every 30 seconds
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { className } = this.props;
    const { data } = this.state;

    if (!data) {
      return null;
    }

    return (
        <div className={className}>
          <Title>Plants</Title>
          <Row type="flex" justify="space-around" align="top">
              {
                Object.keys(data).map(key => {
                  return (
                    <Table key={key} title={key} data={data[key]} />
                  );
                })
              }
          </Row>
        </div>
    );
  }
}

const Title = styled.p`
  font-size: 24px;
`

const StyledMonitor = styled(Monitor)`
  padding: 24;
  background: #666666;
  color: #FFFFFF;
  min-height: 360;
  
  td {
    /* border: 1px solid black; */
    padding:5px;    
  }

  th {
    /* border: 1px solid black; */
    /* background:#4a3a; */
    color:white;
    text-align: center;
    border-radius: 6px;
    padding:5px;
    width:100%;
    font-size: 1.4em;
    /* box-shadow:  3px 20px 5px 0px red; */
  }
`;

export default StyledMonitor;