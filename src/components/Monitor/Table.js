import React from 'react';
import Card from './Card.js'
import { Card as AntCard, Row, Col, Descriptions, Badge, Button } from 'antd';

import styled from 'styled-components';

const cardHeaderStyle = { color: "white", fontSize: "26px", textAlign: "center" };


class Table extends React.Component {

  render() {
    const { className, title, data } = this.props;

    // don't render anything if no data
    if (!data) {
      return null;
    }

    return (
      <Col span={3}>
        <AntCard title={title} bordered={false} headStyle={cardHeaderStyle} className={className}>
          {
            
            Object.keys(data).map(key => {
              return (
                <Card key={key} title={key} data={data[key]} />
                
              );
            })
          
          }
        </AntCard>
        
      </Col>
    );
  }
}


const StyledTable = styled(Table)`
  background: #888888;
  color: white;

  table {
    background:#aaaaaa;
    /* color:rgb(0, 0, 0); */
    border-radius: 10px;
    /* border: 0px solid black; */
    width:100%;
    table-layout: fixed;
    border-collapse: collapse;
    box-shadow: inset  -15px 0px 0px 0px lightgreen, 0px 0px 0px 0px #aaaaaa;
    margin-bottom: 10px;
    /* box-shadow: inset  15px 0px 0px 0px #aaaaaa; */
  }
`

export default StyledTable;