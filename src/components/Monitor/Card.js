import React, { Component } from 'react';
import { Button } from 'antd';
// import './actualMonitor.css';

import styled from 'styled-components';

/*
[
      {
        "Cartons": "123,456",
        "Trailers": "42"
      }
    ]
    */
class MyCard extends Component {
  state = {
    headerActive: false
  };

  handleClick = () => {
    //alert('clicked');
    this.setState({
      headerActive: !this.state.headerActive
    })
  }

  render() {
    const {headerActive} = this.state;
    const {className, title, data} = this.props;

    if (!data || data.length === 0) {
      return null;
    }

    return (
      <table className={className}>
        <thead>
          <TableHeader title={title} isActive={headerActive} onClick={this.handleClick} />
        </thead>
        <tbody>
          {
            data.map(entry => Object.keys(entry).map(key => {
              return (
                <TableRow key={key} title={key} value={entry[key]} />
              );
            }))
          }
        </tbody>
      </table>
    );
  }
}

// dumb component
// const TableHeader = ({title, isActive, onClick}) => (
//   <tr className={isActive ? "green" : "red"}>
//     <th colSpan="2" onClick={onClick}>
//       {title}
//     </th>
//   </tr>
// );

// we can inline style it too!!
// this.props.className needs to be passed for styled-components style to work
// can access props passed directly inside styled components using ${props}
// const TableHeader = styled(({title, className, onClick}) => (
//   <tr className={className}>
//     <th colSpan="2" onClick={onClick}>
//       {title}
//     </th>
//   </tr>
// ))`
//   background: ${props => props.isActive ? 'green' : 'red'}

//   :hover {
//     cursor: pointer;
//   }
// `;

// props blocks
const TableHeader = styled(({title, className, onClick}) => (
  <tr className={className}>
    <th colSpan="2" onClick={onClick}>
      {title}
    </th>
  </tr>
))`
  ${props => props.isActive ? `
    background: green;
    font-size: 16px;
  ` : `
    background: red;
    font-size: 16px;
  `}

  :hover {
    cursor: pointer;
  }
`;

// const TableRow = ({title, value}) => {
//   return (
//     <tr>
//       <TableDesc>{title}</TableDesc>
//       <TableDetail>{value}</TableDetail>
//     </tr>
//   );
// }
// =

// when you just return (); you can directly => (); without the fn body {} and return 
const TableRow = ({title, value}) => (
  <tr>
    <TableDesc>{title}</TableDesc>
    <TableDetail>{value}</TableDetail>
  </tr>
);

// also the above is a function component, shorter version of :
// = 
// class TableRow extends React.Component {
//   render() {
//     const {title, value} = this.props;
//     return (
//       <tr>
//         <TableDesc>{title}</TableDesc>
//         <TableDetail>{value}</TableDetail>
//       </tr>
//     );
//   }
// }

const TableDesc = styled.td`
  background: #aaaaaa;
  border-radius: 0px 0px 0px 6px;
  font-size: 1em;
  width: 100%;
`

const TableDetail = styled.td`
  background: lightGreen;
  color: black;
  border-radius: 0px 0px 6px 0px;
  whitespace: nowrap;
`

export default MyCard;