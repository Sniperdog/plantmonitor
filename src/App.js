// src/App.js
import React, { Component } from 'react';
import ReactDOM from 'react-dom'; 
import { Layout, Menu, Breadcrumb } from 'antd';
import './App.css';
import Monitor from './components/Monitor/Monitor.js'; 
import MainMenu from './components/Menu/MainMenu.js';

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;

class App extends Component {
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <MainMenu />

        <Layout>
          <Header style={{ background: '#fff', padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Full monitoring</Breadcrumb.Item>
              <Breadcrumb.Item>Belgium</Breadcrumb.Item>
            </Breadcrumb>

            <Monitor />

          </Content>
          <Footer style={{ textAlign: 'center' }}>Plant Monitor ©2019 Nike - Created by Michael Hoeykens</Footer>
        </Layout>
      </Layout>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

export default App;